﻿using System;
using System.Collections.Generic;
using System.Text;
using bsa2020_hw1.Models;
namespace bsa2020_hw1.Models
{
    public class TeamPlayers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> ListUser { get; set; }
    }
}
