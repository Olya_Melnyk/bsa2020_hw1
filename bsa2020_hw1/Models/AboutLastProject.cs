﻿using System;
using System.Collections.Generic;
using System.Text;
using bsa2020_hw1.Models;
namespace bsa2020_hw1.Models
{
    public class AboutLastProject
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int CountTasks { get; set; }
        public int CountNotFinishedOrCanceledTasks { get; set; }
        public TaskModel LongestTask { get; set; }
    }
}
