﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bsa2020_hw1.Models
{
    public enum TaskStateModel
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
