﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bsa2020_hw1.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<User> UserList { get; set; }
    }
}
