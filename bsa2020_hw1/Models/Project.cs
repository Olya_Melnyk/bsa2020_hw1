﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace bsa2020_hw1.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public List<TaskModel> Tasks { get; set; } 
        public Team Team { get; set; }
    }
}
