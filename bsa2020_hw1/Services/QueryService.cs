﻿using System;
using System.Collections.Generic;
using System.Text;
using bsa2020_hw1.Models;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
namespace bsa2020_hw1.Services
{
    public class QueryService
    {
        private static readonly HttpService _httpService;

        private static List<Project> _projectList;
        private static List<TaskModel> _taskList;
        private static List<User> _userList;
        private static List<Team> _teamList;

        static QueryService()
        {
            _httpService = new HttpService();
            InitializeLists();
        }

        public static async Task<List<T>> InitializeList<T>(string path)
        {
            try
            {
                 return JsonConvert.DeserializeObject<List<T>>(await _httpService.GetEntities(path));
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);
            }
            return default(List<T>);
        }

        public static async Task<T> InitializeEntity<T>(string path)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(await _httpService.GetEntities(path));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return default(T);
        }
        public static void InitializeLists()
        {
            _taskList = InitializeList<TaskModel>("tasks").Result;
            _projectList =  InitializeList<Project>("projects").Result ;
            _teamList =  InitializeList<Team>("teams").Result;
            _userList =  InitializeList<User>("users").Result;
        }
        public  Dictionary<int, int> GetCountTasksByUser(int authorId)
        {
            return _projectList
                .Where(x => x.AuthorId == authorId)
                .GroupJoin(_taskList,
                p=>p.Id,
                t=>t.ProjectId,
                (p, t) => new {
                    projectId=p.Id,
                    taskListCount=t.Count()
                })
                .ToDictionary(g => g.projectId, g => g.taskListCount);
        }
        public  List<TaskModel> GetTasksForUser(int performerId)
        {
            return _taskList
                .Where(task => task.PerformerId == performerId && task.Name.Length < 45)
                .ToList();
        }

        public  Dictionary<int, string> GetListFinishedTasksAt2020(int performerId)
        {
            return _taskList
                .Where(task => task.PerformerId == performerId && task.FinishedAt.Year == 2020
                && (int)task.State == 2)
                .ToDictionary(task => task.Id, task => task.Name);
        }

        public List<TeamPlayers> GetListUserByTeamAndMoreThenTenYearsOld()
        {
            return _teamList
                .GroupJoin(
                _userList,
                t => t.Id,
                u => u.TeamId,
                (team, userList) =>
                new TeamPlayers()
                {
                    Id = team.Id,
                    Name = team.Name,
                    ListUser = userList.Where(user => user.Birthday.Year < 2010).ToList()
                }).ToList();
        }
        public  List<User> GetListUserByFirstName()
        {
            return _userList.GroupJoin(
                _taskList,
                u => u.Id,
                t => t.PerformerId,
                (u, t) => {
                    u.Tasks = t.OrderByDescending(x=>x.Name.Length).ToList();
                    return u;
                })
                .OrderBy(x=>x.FirstName)
                .ToList();
        }
        public  AboutLastProject GetInfoAboutLastProjectByUserId(int authorId)
        {
            User user = InitializeEntity<User>($"users/{authorId}").Result;
            AboutLastProject project = _taskList
                .Join(
                _projectList,
                t => t.ProjectId,
                p => p.Id,
                (t, p) => new AboutLastProject()
                {
                    User = user,
                    LastProject = _projectList
                    .Where(x => x.AuthorId == authorId)
                    .OrderByDescending(x => x.CreatedAt)
                    .First(),
                    CountTasks = _taskList
                    .Where(task => task.ProjectId == _projectList
                    .Where(x => x.AuthorId == authorId)
                    .OrderByDescending(x => x.CreatedAt)
                    .First().Id
                    ).Count(),
                    CountNotFinishedOrCanceledTasks = _taskList
                    .Where(task => ((int)task.State != 2)
                    && task.PerformerId == authorId)
                    .Count(),
                    LongestTask = _taskList
                    .Where(task => task.PerformerId == authorId)
                    .OrderByDescending(x => x.FinishedAt - x.CreatedAt)
                    .First()
                })
                .First();
            return project;
        }
        public  List<AboutProject> GetInfoAboutProjects()
        {
            var aboutProject = _teamList
                .GroupJoin(
                _userList,
                t => t.Id,
                u => u.TeamId,
                (team, u) =>
                {
                    team.UserList = u.ToList();
                    return team;
                })
                .Join(
                _projectList,
                t => t.Id,
                p => p.TeamId,
                (t, project) =>
                {
                    project.Team = t;
                    return project;
                })
                .GroupJoin(
                _taskList,
                p => p.Id,
                t => t.ProjectId,
                (project, t) =>
                {
                    project.Tasks = t.ToList();
                    return project;
                })
                .Select(
                project => new AboutProject()
                {
                    Project = project,
                    TheLongestTask = project
                    .Tasks?
                    .OrderByDescending(t => t.Description.Length)
                    .FirstOrDefault(),
                    TheShortestTask = project
                    .Tasks?
                    .OrderBy(t => t.Name.Length)
                    .FirstOrDefault(),
                    CountPlayers = project.Tasks.Count < 3
                    || project.Description.Length > 20
                    ? project.Team.UserList?.Count : 0
                }).OrderBy(p=>p.Project.Id).ToList();
        
           return aboutProject;
        }
    }
}
