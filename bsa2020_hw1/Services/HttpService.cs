﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
namespace bsa2020_hw1.Services
{
    public class HttpService
    {
        private static readonly HttpClient _client;
        private const string APP_PATH = "https://bsa20.azurewebsites.net/api/";

        static HttpService()
        {
            _client = new HttpClient();
        }

        public  async Task<string> GetEntities(string path)
        {
            HttpResponseMessage response = await _client.GetAsync(APP_PATH + path);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }
    }
}
